<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Pre-Built Name

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
This Pre-Built enables users to generate an report containing the difference between two Command Template outputs. The main workflow (commandTemplateDiff) consists of two Pre-Built transformations that filter the Command Template Results and create the diff, while a Jinja2 Template formats the filtered information.


<table><tr><td>
  <img src="./images/wf.png" alt="workflow" width="800px">
</td></tr></table>

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`

## Features

The main benefits and features of the Pre-Built are outlined below.

<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->

- Allows users to view the difference between Pre and Post Command Template checks 
- Does not involve a manual task (can run without user intervention)


## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>


## How to Run

This Pre-Built can be used as a Child Job in any existing automation:
- Add a child job task and search for the workflow `commandTemplateDiff`
- Provide the 2 input variables `post` and `pre` (from the output of the runCommandTemplate task) and press save
- The output variable `diff` contains the new-line separated difference between the two inputs
<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

<table><tr><td>
  <img src="./images/wf_input.png" alt="input" width="600px">
</td></tr></table>

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
